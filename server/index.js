const express = require("express")
const app = express()
const WSserver = require('express-ws')(app)
const { v4: uuidv4 } = require('uuid');
const PORT = process.env.PORT || 5000
const clients = {};
app.ws('/', (ws,req) =>{

    const id = uuidv4()
    clients[id] = ws
    ws.on('message', (msg) =>{
        for (const id in clients) {
            clients[id].send(msg)
        }
    })
})

app.listen(PORT, () => {
    console.log(`server started on PORT ${PORT}`)
})
